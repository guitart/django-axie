import logging
from functools import wraps
from jwt.exceptions import PyJWTError

from decoder import jwt_decode_handler
from errors import AuthError
from authentication import JSONWebTokenAuthentication


def verify_querystring_token(f):
    @wraps(f)
    def inner(*args, **kwargs):
        self, request = args
        token = self.request.query_params.get('token')

        if token is None:
            raise AuthError('Unauthorized', status=401)

        user = None
        try:
            token_payload = jwt_decode_handler(token)
            auth_service = JSONWebTokenAuthentication()
            user = auth_service.authenticate_credentials(token_payload)
            if user is None:
                raise AuthError('Forbidden', status=403)
            setattr(request, 'user', user)
            args = self, request
        except PyJWTError as e:
            logging.error(e)
            raise AuthError('Invalid credentials', status=401)
        except Exception as e:
            logging.error(e)
            raise AuthError('Invalid Auth0 credentials', status=403)

        return f(*args, **kwargs)
    return inner
