
import logging
import requests
from requests import exceptions
from django.conf import settings
from rest_framework.exceptions import ValidationError
from os import environ


class Auth0Client(object):

    def _get_token(self):
        url = f'{settings.AUTH0_ISSUER}oauth/token'
        payload = {
            'audience': f'{settings.AUTH0_ISSUER}api/v2/',
            'client_id': settings.AUTH0_M2M_CLIENT_ID,
            'client_secret': settings.AUTH0_M2M_CLIENT_SECRET,
            'grant_type': 'client_credentials',
        }
        response = requests.post(url, json=payload)

        if response.status_code != 200:
            logging.error('Error requesting token from auth0')
            raise requests.exceptions.HTTPError

        return response.json()['access_token']

    def __auth0_search_user_by_user_id(self, auth0_id):
        url = '{auth0_host}api/v2/users/{auth0_id}?search_engine=v3'.format(
            auth0_host=settings.AUTH0_ISSUER,
            auth0_id=auth0_id
        )
        token = self._get_token()
        response = requests.get(url, headers={
            'Authorization': 'Bearer %s' % token
        })

        return {'data': response.json(), 'status_code': response.status_code}

    def get_user(self, auth0_id):
        username, email = None, None
        auth0_data = self.__auth0_search_user_by_user_id(auth0_id).get('data')

        if auth0_data is None:
            return None, None

        email = auth0_data.get('email')
        username = auth0_data.get('name')

        return username, email

    def create_user(self, email, password, blocked=False):
        url = '{auth0_host}api/v2/users'.format(
            auth0_host=settings.AUTH0_ISSUER,
        )
        token = self._get_token()
        data = {
            'email': email,
            'password': password,
            'connection': 'Username-Password-Authentication',
            'verify_email': True,
            'blocked': blocked
            # 'connection': 'email'
        }
        response = requests.post(
            url,
            json=data,
            headers={
                'Authorization': 'Bearer %s' % token
            }
        )
        logging.info('user created: %s - %s', email, response.content)

        if response.status_code != 201:
            raise Exception(str(response.content))

        return {'data': response.json(), 'status_code': response.status_code}

    def trigger_reset_password(self, email):

        url = '{auth0_host}dbconnections/change_password'.format(
            auth0_host=settings.AUTH0_ISSUER,
        )
        token = self._get_token()
        data = {
            'email': email,
            'client_id': environ.get('FRONTEND_AUTH0_CLIENT_ID', '3p5kgeP0tEv6Y2tZikcgzyOSCR2gttI4'),
            'connection': 'Username-Password-Authentication'
        }
        try:
            requests.post(
                url,
                json=data,
                headers={
                    'Authorization': 'Bearer %s' % token
                }
            )
            logging.info('password reset sent: %s', email)
        except exceptions.HTTPError:
            logging.error('Error triggering password reset flow: %s', email)

    def update_user(self, user_id, email, blocked):
        url = '{auth0_host}api/v2/users/{user_id}'.format(
            auth0_host=settings.AUTH0_ISSUER,
            user_id=user_id,
        )
        token = self._get_token()

        data = {
            'email': email,
            'blocked': blocked,
        }

        try:
            requests.patch(
                url,
                json=data,
                headers={
                    'Authorization': 'Bearer %s' % token
                }
            )
            logging.info('update_user: %s - %s', email, user_id)
        except exceptions.HTTPError:
            logging.error('Error triggering password reset flow: %s', email)
