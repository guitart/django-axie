from django.contrib.auth import authenticate
from rest_framework_jwt import authentication


class JSONWebTokenAuthentication(authentication.JSONWebTokenAuthentication):
    def authenticate_credentials(self, payload):
        auth0_id = payload.get('sub')
        return authenticate(remote_user=auth0_id)
