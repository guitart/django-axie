import logging

from django.conf import settings
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.utils import jwt_get_secret_key
from jwt import PyJWKClient

from . import iss_patch

logger = logging.getLogger(__name__)


def jwt_decode_handler(token):
    options = {
        'verify_exp': api_settings.JWT_VERIFY_EXPIRATION,
        'verify_signature': api_settings.JWT_VERIFY
    }
    # get user from token, BEFORE verification, to get user secret key
    url = settings.AUTH0_JWKS_URL
    jwks_client = PyJWKClient(url)
    signing_key = jwks_client.get_signing_key_from_jwt(token)

    # print(api_settings)
    # unverified_payload = iss_patch.decode(token, None, options={"verify_signature": False})
    # logger.exception('==============unverified_payload===============', unverified_payload)
    # secret_key = jwt_get_secret_key(unverified_payload)
    # logger.exception('==============secret_key===============', secret_key)
    # logger.exception(secret_key)
    return iss_patch.decode(
        token,
        signing_key.key,
        algorithms=[api_settings.JWT_ALGORITHM],
        audience=api_settings.JWT_AUDIENCE,
        options=options,
        leeway=api_settings.JWT_LEEWAY,
        issuer=api_settings.JWT_ISSUER,
    )
