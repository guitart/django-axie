from jwt import InvalidIssuerError, MissingRequiredClaimError, PyJWT


class IssArrayPyJWT(PyJWT):
    def _validate_iss(self, payload, issuers):
        if issuers is None:
            return
        if 'iss' not in payload:
            raise MissingRequiredClaimError('iss')

        # This check allows us to validate more than one issuer.
        if payload['iss'] not in issuers:
            raise InvalidIssuerError('Invalid issuer')


_jwt_global_obj = IssArrayPyJWT()
decode = _jwt_global_obj.decode
