import json

import asyncio
import aiohttp

from django.db import models
from django.contrib.auth import get_user_model


SLP_URL = 'https://game-api.skymavis.com/game-api/clients/{}/items/1'

async def get(url, session):
    try:
        async with session.get(url=url) as response:
            resp = await response.read()
        return resp
    except Exception as e:
        print('error', e)

async def get_scholars_data(ronin_wallets):
    urls = map(lambda add: SLP_URL.format(f'0x{add[6:]}'), ronin_wallets)
    async with aiohttp.ClientSession() as session:
        responses = await asyncio.gather(*[get(url, session) for url in urls])
    
    result = [json.loads(res) for res in responses]
    return result

class Scholar(models.Model):
    name = models.CharField(max_length=64)
    address = models.CharField(max_length=46)
    payment_address = models.CharField(max_length=46, blank=True)
    user = models.ForeignKey(get_user_model(), on_delete=models.deletion.CASCADE)

    def __str__(self):
        return f'{self.name} - [{self.address}]'