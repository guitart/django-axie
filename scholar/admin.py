from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from scholar.models import Scholar



class HasPaymentAddressFilter(admin.SimpleListFilter):
    title = _('Has Payment')

    parameter_name = 'has_payment_address'

    def lookups(self, request, model_admin):
        return (
            (1, _('With payment address')),
            (0, _('Without payment address')),
        )

    def queryset(self, request, queryset):
        if self.value() == '1':
            return queryset.exclude(payment_address__exact='')
        if self.value() == '0':
            return queryset.filter(payment_address__exact='')



class ScholarAdmin(admin.ModelAdmin):
    list_display = ('address', 'name', 'payment_address')
    list_filter = (HasPaymentAddressFilter,)

admin.site.register(Scholar, ScholarAdmin)