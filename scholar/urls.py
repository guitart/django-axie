from django.urls import include, path
from rest_framework import routers

from scholar.views import ScholarViewSet

router = routers.DefaultRouter()
router.trailing_slash = '/?'

router.register(r'scholar', ScholarViewSet, basename='scholar')

urlpatterns = [
    path('', include(router.urls))
]