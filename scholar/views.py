import math
from datetime import datetime

from asgiref.sync import async_to_sync
from rest_framework import viewsets, mixins
from rest_framework.request import Request
from rest_framework.response import Response

from scholar.models import Scholar, get_scholars_data
from scholar.serializers import ScholarSerializer

SECONDS_IN_DAY = 86400


class ScholarViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    queryset = Scholar.objects.all()
    serializer_class = ScholarSerializer

    def get_queryset(self):
        qs = self.queryset.filter(user=self.request.user)
        return qs

    
    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        ronin_wallets = map(lambda data:data['address'], response.data)

        data = async_to_sync(get_scholars_data)(ronin_wallets)
        for idx, scholar in enumerate(response.data):
            slp = data[idx]['total']
            last_claimed = data[idx]['last_claimed_item_at']
            days_since = datetime.now() - datetime.fromtimestamp(last_claimed)
            days_since = days_since.total_seconds() / SECONDS_IN_DAY
            days_since = math.ceil(days_since)
            average = slp / days_since
            scholar['slp'] = slp
            scholar['last_claimed_slp_at'] = last_claimed
            scholar['days_since_claimed'] = days_since
            scholar['slp_average'] = round(average, 2)
        return response