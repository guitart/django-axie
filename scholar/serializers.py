from django.db.models import fields
from rest_framework import serializers
from scholar.models import Scholar


class ScholarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scholar
        fields = '__all__'