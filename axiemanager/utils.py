import requests
from cryptography.hazmat.backends import default_backend
from cryptography.x509 import load_pem_x509_certificate

UUID_REGEX = (
    r'[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}'
)


def jwt_public_key(url):
    if not url:
        return ''
    response = requests.get(url)
    jwks = response.json()
    cert = f"-----BEGIN CERTIFICATE-----\n{jwks['keys'][0]['x5c'][0]}\n-----END CERTIFICATE-----"
    certificate = load_pem_x509_certificate(cert.encode(), default_backend())
    return certificate.public_key()