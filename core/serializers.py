from django.contrib.auth import get_user_model
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    user_id = serializers.UUIDField(source='id', read_only=True)
    username = serializers.CharField()
    email = serializers.CharField()
    auth0_id = serializers.CharField()

    class Meta:
        model = get_user_model()
        fields = ['user_id', 'username', 'email', 'created_datetime', 'auth0_id']
        extra_kwargs = {'created_datetime': {'read_only': True}}


class PasswordSerializer(serializers.Serializer):

    new_password = serializers.CharField(required=True)
