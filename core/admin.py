from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.utils.translation import gettext as _
from simple_history.admin import SimpleHistoryAdmin

from core.models import User

class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = '__all__'

class UserAdmin(BaseUserAdmin, SimpleHistoryAdmin):
    ordering = ['id']
    list_display = ['email', 'auth0_id', 'full_name', 'username']
    fieldsets = (
        (None, {'fields': ('auth0_id', 'email', 'username', 'password',)}),
        (_('Personal Info'), {'fields': ('full_name',)}),
        (
            _('Persmissions'),
            {
                'fields': (
                    'is_active',
                    'is_staff',
                    'is_superuser',
                    'groups',
                    'user_permissions',
                )
            }
        ),
        (_('Important Dates'), {'fields': ('last_login',)})
    )
    form = CustomUserChangeForm
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')
        }),
    )

admin.site.register(User, UserAdmin)