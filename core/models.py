import uuid

from django.db import models, IntegrityError
from django.db.models import signals
from django.dispatch import receiver
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager,\
                                       PermissionsMixin, Permission

from django.contrib.contenttypes.models import ContentType

from simple_history.models import HistoricalRecords

from auth0.client import Auth0Client

class BaseModel(models.Model):
    """Base model, contains all fields models should have"""
    uuid = models.UUIDField(
        unique=True,
        editable=False,
        default=uuid.uuid4,
        verbose_name='Public Identifier'
    )
    created_datetime = models.DateTimeField(
        'Created at',
        null=True, blank=True,
        auto_now_add=True
    )
    modified_datetime = models.DateTimeField(
        'Modified at',
        null=True, blank=True,
        auto_now=True
    )
    history = HistoricalRecords(inherit=True)

    class Meta:
        abstract = True


class UserManager(BaseUserManager):
    use_in_migrations = True

    def get_or_create(self, defaults=None, **kwargs):
        """
        Look up an object with the given kwargs, creating one if necessary.
        Return a tuple of (object, created), where created is a boolean
        specifying whether an object was created.
        """
        # The get() needs to be targeted at the write database in order
        # to avoid potential transaction consistency problems.
        defaults = defaults or {}
        self._for_write = True

        try:
            return self.get(**kwargs), False
        except self.model.DoesNotExist:
            try:
                new_user_data = {**kwargs, **defaults}
                return self.create_user(**new_user_data), True
            except IntegrityError:
                # hack for race condition
                return self.get(**kwargs), False

    def create_user(self, email, password=None, **extra_fields):
        """Creates and saves a new user"""
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None, **extra_fields):
        """Creates and  saves a super user"""
        user = self.create_user(
            email=email,
            password=password,
            is_superuser=True,
            is_staff=True
        )
        return user


class NullableCharField(models.CharField):
    def to_python(self, value):
        if isinstance(value, str):
            return value
        return value or ''

    def get_prep_value(self, value):
        return value or None


class User(AbstractBaseUser, PermissionsMixin, BaseModel):
    """Custom user that supports email"""
    auth0_id = models.CharField(max_length=128, null=True, unique=True, blank=True)
    email = models.EmailField(max_length=255, unique=True, null=False)
    full_name = models.CharField(blank=True, max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    objects = UserManager()
    username = NullableCharField(
        max_length=50, unique=True,
        null=True, blank=True
    )

    USERNAME_FIELD = 'email'

    def add_permissions(self, *permissions):
        for permission in permissions:
            [app_label, code_name] = permission.split('.')
            model = code_name.split('_')[1]

            content_type = ContentType.objects.get(
                app_label=app_label,
                model=model
            )
            permission = Permission.objects.get(
                content_type=content_type.id,
                codename=code_name
            )
            self.user_permissions.add(permission)

    def save(self, *args, **kwargs):
        self.email = self.email.lower()
        return super().save(*args, **kwargs)


@receiver(signals.pre_save, sender=User)
def create_auth0_account(sender, instance, **kwargs):
    """
        Triggered when an user is created to be replicated in auth0
    """
    blocked = True

    if not instance._state.adding or settings.DEBUG:
        return

    if instance.is_superuser or instance.is_staff:
        return

    user_profile = getattr(instance, 'profile', {})
    user_company = getattr(user_profile, 'company', None)

    if user_company:
        if instance.is_active and user_company.is_active:
            blocked = False
        else:
            blocked = True
    else:
        blocked = True

    auth0_client = Auth0Client()
    result = auth0_client.create_user(
        instance.email, instance._password, blocked=blocked)
    user = result.get('data', {})
    auth0_id = user.get('user_id')
    instance.auth0_id = auth0_id
    auth0_client.trigger_reset_password(instance.email)


@receiver(signals.post_save, sender=User)
def update_auth0_account(sender, instance, created, **kwargs):
    """
        Triggered every time an user is updated to sync with auth0 users
    """
    user_company = None
    blocked = True
    auth0_client = Auth0Client()

    if not hasattr(instance, 'profile'):
        return

    user_profile = instance.profile
    user_company = getattr(user_profile, 'company', False)

    if user_company:
        if user_company.is_active and instance.is_active:
            blocked = False
        else:
            blocked = True
    else:
        blocked = True

    auth0_client.update_user(
        instance.auth0_id, instance.email, blocked)
