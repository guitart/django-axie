from django.contrib.auth import get_user_model
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from rest_framework.response import Response

from core.serializers import UserSerializer, PasswordSerializer


User = get_user_model()

# Create your views here.
class UserViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_object(self):
        user_pk = self.kwargs[self.lookup_field]

        if self.request.user.is_superuser:
            try:
                return User.objects.get(pk=user_pk)
            except User.DoesNotExist:
                raise status.HTTP_404_NOT_FOUND

        if user_pk != str(self.request.user.id):
            raise PermissionDenied('User not authorized to operate on this resource')
        return self.request.user

    @action(
        detail=True,
        methods=['post'],
        url_path='_change-password',
        permission_classes=[IsAdminUser],
    )
    def set_password(self, request, pk=None):
        user = self.get_object()
        serializer = PasswordSerializer(data=request.data)

        if serializer.is_valid():
            user.set_password(serializer.validated_data['new_password'])
            user.save()
            return Response({'status': 'password set'})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['GET'], permission_classes=[IsAuthenticated])
    def me(self, request):
        user = request.user
        serializer = UserSerializer(instance=user)
        return Response(serializer.data)