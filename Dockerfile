FROM python:3.10-slim

ENV PYTHONUNBUFFERED True


ENV APP_HOME /app
WORKDIR $APP_HOME
COPY requirements.dev.txt requirements.dev.txt
RUN pip install -r requirements.dev.txt


COPY . ./
ENV PORT 8080

CMD exec gunicorn --bind :$PORT axiemanager.wsgi:application
