# Axie Django

## Quick Start

```sh
python -m venv venv
. ./venv/bin/activate
pip install -r requirements
python manage.py migrate
python manage.py createsuperuser
```
